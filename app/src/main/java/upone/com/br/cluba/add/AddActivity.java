package upone.com.br.cluba.add;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.acra.ACRA;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import io.swagger.client.ApiException;
import io.swagger.client.JsonUtil;
import io.swagger.client.api.DefaultApi;
import io.swagger.client.model.ItemsComanda;
import io.swagger.client.model.Produtos;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import upone.com.br.cluba.R;
import upone.com.br.cluba.category.CategoryActivity;
import upone.com.br.cluba.database.ClubADB;
import upone.com.br.cluba.database.ConfigUtil;
import upone.com.br.cluba.database.TableInfo;
import upone.com.br.cluba.main.MainActivity;
import upone.com.br.cluba.user.UserActivity;

public class AddActivity extends AppCompatActivity implements AdapterView.OnItemClickListener,
        AdapterView.OnItemLongClickListener {

    /**
     * Static attributes
     */
    public static final int CATEGORY_OK = 1;
    private static final String TAG = "AddActivity";
    // private static final
    /**
     * Screen attributes
     */
    private String mNumBarTab;
    private FloatingActionButton newFAB;
    private FloatingActionButton closeFAB;
    private FloatingActionButton listFAB;
    private ItemsComanda mBarTab;
    private List<Produtos> mProductsList;
    private Long mProdId;
    private Integer mQty;
    private Long mCategoryId;
    private String mPicture;
    private boolean[] mIsSent = {false};
    private ClubADB mClubADB;
    private ProgressDialog mProgressDialog;
    private ArrayList<ItemsComanda> mItemsComandas;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        //Getting bundle values
        final Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mNumBarTab = extras.getString(MainActivity.NUM_BAR_TAB);
            mProdId = Long.parseLong(extras.getString(SearchViewActivity.PRODUCT_ID));
            mCategoryId = Long.parseLong(extras.getString(SearchViewActivity.CATEGORY_ID));
            mPicture = extras.getString(SearchViewActivity.PICTURE);
            final String qty = extras.getString(QtyComplActivity.QTY);
            if (qty != null && !qty.isEmpty()) {
                mQty = Integer.valueOf(qty);
            } else {
                mQty = 1;
            }
        }
        setup();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateBarTab();
    }

    /**
     * Setup
     */
    private void setup() {

        // O titulo eh o numero da comanda.
        mNumBarTab = getIntent().getExtras().getString(MainActivity.NUM_BAR_TAB);
        setTitle("Nº Comanda: " + mNumBarTab);

        mProdId = Long.parseLong(getIntent().getExtras().getString(SearchViewActivity.PRODUCT_ID));
        mCategoryId = Long.parseLong(getIntent().getExtras().getString(SearchViewActivity.CATEGORY_ID));
        mQty = Integer.valueOf(getIntent().getExtras().getString(QtyComplActivity.QTY));
        mPicture = getIntent().getExtras().getString(SearchViewActivity.PICTURE);

        // Components
        if (newFAB == null) {
            newFAB = (FloatingActionButton) findViewById(R.id.fabNew);
            newFAB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startCategories();
                }
            });
        }

        // Components
        if (listFAB == null) {
            listFAB = (FloatingActionButton) findViewById(R.id.fabList);
            listFAB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startListComanda();
                }
            });
        }

        if (closeFAB == null) {
            closeFAB = (FloatingActionButton) findViewById(R.id.fabClose);
            closeFAB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendBarTab();
                }
            });
        }

    }

    /**
     * Chama a comanda com os itens que foram adicionados.
     */
    private void startListComanda() {
        final Intent intent = new Intent(this, BarTabListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(MainActivity.NUM_BAR_TAB, mNumBarTab);
        startActivityForResult(intent, CATEGORY_OK);
    }

    /**
     * Inicia a activity de categories.
     */
    private void startCategories() {
        final Intent intent = new Intent(this, CategoryActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(MainActivity.NUM_BAR_TAB, mNumBarTab);
        startActivityForResult(intent, CATEGORY_OK);
    }

    /**
     * Atualiza os itens da comanda e a lista de itens da tela.
     */
    protected void updateBarTab() {
        final AsyncTask<String, Void, String> asyncTask = new AsyncTask<String, Void, String>() {

            @SuppressWarnings("unchecked")
            @Override
            protected String doInBackground(String... params) {
                Produtos produtos = findItem();
                if (produtos == null) {
                    produtos = findItemAtList();
                }
                try {
                    mBarTab = (ItemsComanda) getmClubADB().getObject(TableInfo.ITEMS_COMANDA.name(), ItemsComanda.class);
                    mItemsComandas = (ArrayList<ItemsComanda>)
                            getmClubADB().getListObject(TableInfo.ITEMS_COMANDA_LIST.name(), ItemsComanda.class);
                    getCurrentComandaPessoaId();
                    boolean hasItem = false;
                    if (produtos != null) {
                        for (ItemsComanda comanda : mItemsComandas) {
                            if (comanda.getProdutoID().equals(produtos.getProdutoID())) {
                                comanda.setQuantidade(Long.valueOf(mQty));
                                comanda.setCategoriaID(mCategoryId);
                                hasItem = true;
                                break;
                            }
                        }
                        if (!hasItem) {
                            ItemsComanda itemsComanda = new ItemsComanda();
                            itemsComanda.setDescricao(produtos.getNome());
                            itemsComanda.setProdutoID(produtos.getProdutoID());
                            itemsComanda.setQuantidade(Long.valueOf(mQty));
                            itemsComanda.setCategoriaID(mCategoryId);
                            itemsComanda.setValorProduto(produtos.getValor());
                            if (mBarTab != null) {
                                itemsComanda.setComandaID(mBarTab.getComandaID());
                                itemsComanda.setComandaItem(mBarTab.getComandaItem());
                                itemsComanda.setComandaPessoaID(mBarTab.getComandaPessoaID());
                                itemsComanda.setEstacaoTrabalho(mBarTab.getEstacaoTrabalho());
                                itemsComanda.setUsuario(mBarTab.getUsuario());
                            }
                            mItemsComandas.add(itemsComanda);
                        }
                        if (mProductsList == null) {
                            mProductsList = (List<Produtos>) getmClubADB().getListObject(TableInfo.PRODUCTS_BAR_TAB.name(),
                                    Produtos.class);
                        }
                        for (Produtos p : mProductsList) {
                            if (p.getProdutoID().equals(produtos.getProdutoID())) {
                                mProductsList.remove(p);
                                break;
                            }
                        }
                        mProductsList.add(produtos);
                        getmClubADB().remove(TableInfo.PRODUCTS_BAR_TAB.name());
                        getmClubADB().putListObject(TableInfo.PRODUCTS_BAR_TAB.name(), (ArrayList<?>) mProductsList);

                    }

                    getmClubADB().remove(TableInfo.ITEMS_COMANDA_LIST.name());
                    getmClubADB().putListObject(TableInfo.ITEMS_COMANDA_LIST.name(), mItemsComandas);
                } catch (IOException e1) {
                    Log.e(TAG, e1.getMessage(), e1);
                }
                return String.valueOf(mBarTab);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                fillList();
            }
        };

        asyncTask.execute(TableInfo.ITEMS_COMANDA.name());
    }

    private void getCurrentComandaPessoaId() {
        if (mBarTab.getComandaPessoaID() == null) {
            DefaultApi defaultApi = new DefaultApi();
            ConfigUtil.getInstance().setContext(AddActivity.this);
            defaultApi.setBasePath(ConfigUtil.getInstance().getBasePath() + ConfigUtil.getInstance().getApi());
            String response = null;
            try {
                response = defaultApi.getItensComanda(Long.valueOf(mNumBarTab));
            } catch (TimeoutException | ExecutionException | ApiException | InterruptedException e) {
                Log.e(TAG, e.getMessage(), e);
            }
            if (response != null) {
                List<ItemsComanda> itemsComandas = JsonUtil.deserializeToList(response, ItemsComanda.class);
                if (!itemsComandas.isEmpty()) {
                    if (mBarTab != null && mBarTab.getComandaPessoaID() == null) {
                        for (int i = 0; i < itemsComandas.size(); i++) {
                            if (itemsComandas.get(i).getComandaPessoaID() != null) {
                                mBarTab.setComandaPessoaID(itemsComandas.get(i).getComandaPessoaID());
                                break;
                            }
                        }
                    }
                    getmClubADB().remove(TableInfo.ITEMS_COMANDA.name());
                    getmClubADB().putObject(TableInfo.ITEMS_COMANDA.name(), mBarTab);
                }
            }
        }
    }


    /**
     * Encontra o produto para edicao on Item Click e on Long Item.
     *
     * @return Instancia de Produtos
     */
    @SuppressWarnings("unchecked")
    private Produtos findItemAtList() {
        // encontra o produto
        if (mProdId > -1L) {
            // busca o novo produto
            try {
                mProductsList = (List<Produtos>) getmClubADB().getListObject(TableInfo.PRODUCTS_BAR_TAB.name(), Produtos.class);
            } catch (IOException e) {
                Log.e(TAG, e.getMessage(), e);
            }
            for (int i = 0; i < mProductsList.size(); i++) {
                Produtos product = mProductsList.get(i);
                if (product.getProdutoID().equals(mProdId)) {
                    if (product.getFoto() == null) {
                        product.setFoto(mPicture);
                    }
                    return product;
                }
            }
        }
        return null;
    }

    /**
     * Encontra um produto e retorna uma instancia de Produtos.
     *
     * @return Produtos
     */
    @SuppressWarnings("unchecked")
    private Produtos findItem() {
        try {

            //obtem a lista de produtos da categoria informada
            final List<Produtos> productList =
                    (ArrayList<Produtos>) getmClubADB().getListObject(TableInfo.PRODUCTS.name(), Produtos.class);

            // encontra o produto
            if (mProdId > -1L) {
                // busca o novo produto
                for (int i = 0; i < productList.size(); i++) {
                    Produtos product = productList.get(i);
                    if (product.getProdutoID().equals(mProdId)) {
                        if (product.getFoto() == null) {
                            product.setFoto(mPicture);
                        }
                        return product;
                    }
                }
            }
        } catch (IOException e1) {
            Log.e(TAG, e1.getMessage(), e1);
        }
        return null;
    }

    synchronized private void fillList() {
        if (mProductsList != null) {
            String[] mProductNames = new String[mProductsList.size()];
            String[] mQuantities = new String[mProductsList.size()];
            String[] mProfilePics = new String[mProductsList.size()];
            String[] mPrizes = new String[mProductsList.size()];
            int i = 0;
            if (mItemsComandas != null) {
                for (Produtos product : mProductsList) {
                    mProductNames[i] = product.getNome();
                    mQuantities[i] = String.valueOf(mItemsComandas.get(i).getQuantidade());
                    mProfilePics[i] = product.getFoto();
                    mPrizes[i] = String.valueOf(mItemsComandas.get(i).getValorProduto());
                    i++;
                }
            }

            // Adiciona os produtos na tela de busca
            ArrayList<RowItem> mRowItems = new ArrayList<>();
            for (i = 0; i < mProductNames.length; i++) {
                RowItem item = new RowItem(mProductNames[i],
                        mProfilePics[i], mQuantities[i], "R$" + mPrizes[i]);
                mRowItems.add(item);
            }

            ListView mListView = (ListView) findViewById(R.id.listViewResult);
            CustomAdapter mAdapter = new CustomAdapter(AddActivity.this, R.layout.row_itens_2, mRowItems);
            mListView.setAdapter(mAdapter);
            mListView.setOnItemClickListener(AddActivity.this);
            mListView.setOnItemLongClickListener(AddActivity.this);
        }
    }


    /**
     * Envia a comanda para o servidor.
     *
     * @return true se enviado com sucesso e false se for o contrario
     */
    protected boolean sendBarTab() {
        mIsSent[0] = false;
        final AsyncTask<String, Void, String> asyncTask = new AsyncTask<String, Void, String>() {

            @Override
            protected void onPreExecute() {
                mProgressDialog = new ProgressDialog(AddActivity.this);
                mProgressDialog.setCancelable(false);
                mProgressDialog.setMessage(getString(R.string.send_bar_tab));
                mProgressDialog.show();
                super.onPreExecute();
            }

            @SuppressWarnings("unchecked")
            @Override
            protected String doInBackground(String... params) {
                String response = "200";
                try {
                    mItemsComandas = (ArrayList<ItemsComanda>)
                            getmClubADB().getListObject(TableInfo.ITEMS_COMANDA_LIST.name(), ItemsComanda.class);
                    if (mItemsComandas != null) {
                        JsonArray jsonArray = new JsonArray();
                        for (ItemsComanda itemsComanda : mItemsComandas) {
                            if (itemsComanda.getComandaPessoaID() == null) {
                                if (mBarTab.getComandaPessoaID() != null) {
                                    itemsComanda.setComandaPessoaID(mBarTab.getComandaPessoaID());
                                } else {
                                    getCurrentComandaPessoaId();
                                    mBarTab = (ItemsComanda) getmClubADB().getObject(TableInfo.ITEMS_COMANDA.name(),
                                            ItemsComanda.class);
                                    itemsComanda.setComandaPessoaID(mBarTab.getComandaPessoaID());
                                }
                            } else {
                                itemsComanda.setComandaPessoaID(mBarTab.getComandaPessoaID());
                            }
                            if (itemsComanda.getCategoriaID() == null) {
                                itemsComanda.setCategoriaID(mCategoryId);
                            }
                            if (itemsComanda.getUsuario() == null) {
                                itemsComanda.setUsuario(mBarTab.getUsuario());
                            }
                            jsonArray.add(JsonUtil.getGson().toJsonTree(itemsComanda));
                        }
                        JsonObject jsonObject = new JsonObject();
                        jsonObject.add("ItensComanda", jsonArray);
                        final String json = jsonObject.toString();
                        if (ConfigUtil.IS_LOGGING_ENABLED) {
                            final ArrayList<String> listString = getmClubADB().getListString(TableInfo.JSON_LOG.name());
                            listString.add(json);
                            getmClubADB().putListString(TableInfo.JSON_LOG.name(), listString);
                        }
                        ConfigUtil.getInstance().setContext(AddActivity.this);
                        final String path = ConfigUtil.getInstance().getBasePath()
                                + ConfigUtil.getInstance().getApi() +
                                "/ItensComanda";
                        OkHttpClient client = new OkHttpClient();
                        Request request = new Request.Builder().url(path).
                                post(RequestBody.create(MediaType.parse("application/json; charset=utf-8"),
                                        json)).build();
                        Response responseP = client.newCall(request).execute();
                        if (responseP.code() != 200) {
                            mIsSent[0] = false;
                        } else {
                            Log.i(TAG, responseP.body().toString());
                            mIsSent[0] = true;
                        }
                    }

                } catch (Exception e) {
                    mIsSent[0] = false;
                    ACRA.getErrorReporter().handleException(e);
                }
                return response;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if (mProgressDialog != null) {
                    mProgressDialog.dismiss();
                }
                AlertDialog.Builder dialog = new AlertDialog.Builder(AddActivity.this, R.style.Theme_AppCompat_DayNight_Dialog);
                dialog.setCancelable(false);
                if (mIsSent[0]) {
                    getmClubADB().remove(TableInfo.ITEMS_COMANDA.name());
                    dialog.setMessage(getString(R.string.bartab_success));
                    dialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                } else {
                    dialog.setMessage(getString(R.string.error_sending_message));
                    dialog.setPositiveButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                }
                dialog.create().show();
            }
        };
        asyncTask.execute(TableInfo.ITEMS_COMANDA.name());
        return mIsSent[0];
    }

    @Override
    public void finish() {
        super.finish();
        cleanDB();
        Intent intent = new Intent(this, UserActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    /**
     * Limpa o db.
     */

    private void cleanDB() {
        getmClubADB().remove(TableInfo.PRODUCTS.name());
        getmClubADB().remove(TableInfo.ITEMS_COMANDA.name());
        getmClubADB().remove(TableInfo.CATEGORIES.name());
        getmClubADB().remove(TableInfo.PRODUCTS_BAR_TAB.name());
        getmClubADB().remove(TableInfo.ITEMS_COMANDA_LIST.name());
        getmClubADB().remove(TableInfo.BARTENDERS.name());
    }

    @Override
    public void onBackPressed() {
        if (!mIsSent[0]) {
            new AlertDialog.Builder(new ContextThemeWrapper(this, android.R.style.Theme_Black)).setTitle(R.string.send_or_cancel_bar_tab).
                    setPositiveButton(R.string.send, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            sendBarTab();
                        }
                    }).
                    setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    }).create().show();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final Intent intent = new Intent(AddActivity.this, QtyComplActivity.class);
        intent.putExtra(MainActivity.NUM_BAR_TAB, mNumBarTab);
        intent.putExtra(SearchViewActivity.PRODUCT_ID, mProductsList.get(position).getProdutoID());
        intent.putExtra(SearchViewActivity.CATEGORY_ID, String.valueOf(mItemsComandas.get(position).getCategoriaID()));
        intent.putExtra(QtyComplActivity.QTY, mItemsComandas.get(position).getQuantidade());
        startActivityForResult(intent, SearchViewActivity.ACTION_QTY_COMP);
    }

    @Override
    public boolean onItemLongClick(final AdapterView<?> parent, View view, final int position, long id) {
        new AlertDialog.Builder(new ContextThemeWrapper(this, android.R.style.Theme_Black)).setTitle(R.string.exclude_item).
                setPositiveButton(R.string.exclude, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mProductsList.remove(position);
                        mItemsComandas.remove(position);
                        getmClubADB().remove(TableInfo.PRODUCTS_BAR_TAB.name());
                        getmClubADB().remove(TableInfo.ITEMS_COMANDA_LIST.name());
                        if (!mProductsList.isEmpty()) {
                            getmClubADB().putListObject(TableInfo.PRODUCTS_BAR_TAB.name(), (ArrayList<?>) mProductsList);
                        }
                        if (!mItemsComandas.isEmpty()) {
                            getmClubADB().putListObject(TableInfo.ITEMS_COMANDA_LIST.name(), mItemsComandas);
                        }
                        if (mProductsList.isEmpty() && mItemsComandas.isEmpty()) {
                            mProdId = -1L;
                        }
                        fillList();
                        Toast.makeText(AddActivity.this, R.string.removed_item, Toast.LENGTH_SHORT).show();
                    }
                }).
                setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).create().show();
        return true;
    }

    /**
     * Retorna uma instancia do banco
     *
     * @return ClubADB
     */
    private ClubADB getmClubADB() {
        if (mClubADB == null) {
            mClubADB = new ClubADB(AddActivity.this);
        }
        return mClubADB;
    }
}
