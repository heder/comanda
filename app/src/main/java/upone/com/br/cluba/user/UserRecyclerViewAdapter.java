package upone.com.br.cluba.user;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import upone.com.br.cluba.R;
import upone.com.br.cluba.main.MainActivity;

/**
 * UserRecyclerViewAdapter
 * <p>
 * Created by HEDER GUERREIRO on 27/03/2017.
 */

public class UserRecyclerViewAdapter extends RecyclerView.Adapter<UserRecyclerViewAdapter.ViewHolder> {

    public static final String NUM_ID_BARTENDER = "NUM_ID_BARTENDER";
    private Context mContext;
    private String[] mNames;
    private Long[] mValuesBartIds;

    UserRecyclerViewAdapter(UserActivity userActivity, String[] mNames, Long[] mBartenderIds) {
        this.mContext = userActivity;
        this.mNames = mNames;
        this.mValuesBartIds = mBartenderIds;
    }

    @Override
    public UserRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view1 = LayoutInflater.from(mContext).inflate(R.layout.recycler_view_items_2, parent, false);
        return new ViewHolder(view1);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onBindViewHolder(final UserRecyclerViewAdapter.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.mTextView.setText(mNames[position]);
        holder.mTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(mContext, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(NUM_ID_BARTENDER, String.valueOf(mValuesBartIds[position]));
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mNames.length;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTextView;

        ViewHolder(View itemView) {
            super(itemView);
            mTextView = (TextView) itemView.findViewById(R.id.textView);
        }
    }
}