package upone.com.br.cluba.add;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;

import upone.com.br.cluba.R;
import upone.com.br.cluba.main.MainActivity;

public class QtyComplActivity extends AppCompatActivity {

    private TextView qtyTV;
    private TextView mTextView;
    private FloatingActionButton closeFAB;
    private Button mLeftButton;
    private Button mRightButton;
    private String mNumBarTab;
    private String mProductId;
    private String mCategoryId;
    private String mPicture;
    public static final String QTY = "QTY";
    private Long mQty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qty_compl);
        if (getIntent().getExtras() != null) {
            mNumBarTab = getIntent().getExtras().getString(MainActivity.NUM_BAR_TAB);
            mProductId = String.valueOf(getIntent().getExtras().getLong(SearchViewActivity.PRODUCT_ID));
            mCategoryId = getIntent().getExtras().getString(SearchViewActivity.CATEGORY_ID);
            mPicture = getIntent().getExtras().getString(SearchViewActivity.PICTURE);
            mQty = getIntent().getExtras().getLong(QTY);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setup();
    }

    /**
     * Inicia componentes.
     */
    private void setup() {

        if (mNumBarTab == null || mProductId == null || mQty == null || mCategoryId == null
                || mPicture == null) {
            mNumBarTab = getIntent().getExtras().getString(MainActivity.NUM_BAR_TAB);
            mProductId = String.valueOf(getIntent().getExtras().getLong(SearchViewActivity.PRODUCT_ID));
            mCategoryId = getIntent().getExtras().getString(SearchViewActivity.CATEGORY_ID);
            mPicture = getIntent().getExtras().getString(SearchViewActivity.PICTURE);
            mQty = getIntent().getExtras().getLong(QTY);
        }

        if (mTextView == null) {
            mTextView = (TextView) findViewById(R.id.inf_name_tv);
            mTextView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Raleway-SemiBoldItalic.ttf"));
        }

        if (qtyTV == null) {
            qtyTV = (TextView) findViewById(R.id.qtyTV);
            qtyTV.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Raleway-Bold.ttf"));
            qtyTV.setText("1");
            if (mQty > 0) {
                qtyTV.setText(String.valueOf(mQty));
            }
        }

        if (closeFAB == null) {
            closeFAB = (FloatingActionButton) findViewById(R.id.fabClose);
            closeFAB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (qtyTV.getText().length() > 0 && !qtyTV.getText().toString().equals("0")) {
                        finish();
                        Toast.makeText(QtyComplActivity.this, "Comanda atualizada!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(QtyComplActivity.this, AddActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.putExtra(MainActivity.NUM_BAR_TAB, mNumBarTab);
                        intent.putExtra(SearchViewActivity.PRODUCT_ID, mProductId);
                        intent.putExtra(SearchViewActivity.CATEGORY_ID, mCategoryId);
                        intent.putExtra(SearchViewActivity.PICTURE, mPicture);
                        intent.putExtra(QTY, (qtyTV.getText() != null
                                && qtyTV.getText().toString().length() > 0 &&
                                !qtyTV.getText().toString().equals("null") ? qtyTV.getText().toString() : "0"));
                        startActivity(intent);
                    }
                }
            });
        }

        if (mLeftButton == null) {
            mLeftButton = (Button) findViewById(R.id.leftButton);
            mLeftButton.setLongClickable(true);
            mLeftButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mQty = Long.valueOf(qtyTV.getText().toString());
                    mQty--;
                    if (mQty < 1) {
                        mQty = 1L;
                    }
                    qtyTV.setText(String.valueOf(mQty));
                }
            });

            mLeftButton.setOnLongClickListener(new View.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {
                    mQty = Long.valueOf(qtyTV.getText().toString());
                    mQty--;
                    if (mQty < 1) {
                        mQty = 1L;
                    }
                    qtyTV.setText(String.valueOf(mQty));
                    return true;
                }
            });
        }

        if (mRightButton == null) {
            mRightButton = (Button) findViewById(R.id.rightButton);
            mRightButton.setLongClickable(true);
            mRightButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mQty = Long.valueOf(qtyTV.getText().toString());
                    if (mQty < 999) {
                        mQty++;
                    }
                    qtyTV.setText(String.valueOf(mQty));
                }
            });

            mRightButton.setOnLongClickListener(new View.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {
                    mQty = Long.valueOf(qtyTV.getText().toString());
                    mQty = Long.valueOf(qtyTV.getText().toString());
                    if (mQty < 999) {
                        mQty++;
                    }
                    qtyTV.setText(String.valueOf(mQty));
                    return true;
                }
            });
        }

    }
}
