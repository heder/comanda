package upone.com.br.cluba.category;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import io.swagger.client.ApiException;
import io.swagger.client.api.DefaultApi;
import io.swagger.client.model.Categorias;
import upone.com.br.cluba.R;
import upone.com.br.cluba.database.ClubADB;
import upone.com.br.cluba.database.ConfigUtil;
import upone.com.br.cluba.database.TableInfo;
import upone.com.br.cluba.main.MainActivity;


public class CategoryActivity extends AppCompatActivity {

    private static final String TAG = CategoryActivity.class.getName();
    private static final int NUMBER_COLUMNS = 2;
    private String mNumBarTab;
    private List<Categorias> mCategoryList;
    private Long[] mCategoryIds;
    private ProgressDialog mProgressDialog;
    private RecyclerView mRecyclerView;
    private String[] mCategories;
    private Bitmap[] mCategoriesImgs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        setTitle(getString(R.string.choose_category));
        final Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mNumBarTab = extras.getString(MainActivity.NUM_BAR_TAB);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mNumBarTab = getIntent().getExtras().getString(MainActivity.NUM_BAR_TAB);
        setup();
    }

    protected void setup() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view1);
        GridLayoutManager mRecyclerViewLayoutManager = new GridLayoutManager(CategoryActivity.this, NUMBER_COLUMNS);
        mRecyclerView.setLayoutManager(mRecyclerViewLayoutManager);
        getCategories();
    }

    /**
     * Metodo que obtem as categorias
     */
    protected void getCategories() {
        final AsyncTask<String, Void, String> asyncTask = new AsyncTask<String, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(CategoryActivity.this);
                mProgressDialog.setMessage(getString(R.string.searching));
                mProgressDialog.show();
            }

            @SuppressWarnings("unchecked")
            @Override
            protected String doInBackground(String... params) {
                final DefaultApi defaultApi = new DefaultApi();
                String response = null;
                try {
                    ConfigUtil.getInstance().setContext(CategoryActivity.this);
                    defaultApi.setBasePath(ConfigUtil.getInstance().getBasePath()
                            + ConfigUtil.getInstance().getApi());
                    mCategoryList = defaultApi.getCategorias();
                    if (mCategoryList != null && !mCategoryList.isEmpty()) {
                        final ClubADB clubADB = new ClubADB(CategoryActivity.this);

                        // Limpa as categorias que estiverem previamente armazenadas.
                        if (clubADB.getListObject(TableInfo.CATEGORIES.name(), Categorias.class) != null) {
                            clubADB.remove(TableInfo.CATEGORIES.name());
                        }
                        clubADB.putListObject(TableInfo.CATEGORIES.name(), (ArrayList<?>) mCategoryList);
                        response = "200";
                    }
                } catch (TimeoutException | ExecutionException | ApiException | InterruptedException | IOException e) {
                    Log.e(TAG, e.getMessage(), e);
                }
                return response;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if (mProgressDialog != null) {
                    mProgressDialog.dismiss();
                }
                if (mCategoryList != null) {
                    mCategories = new String[mCategoryList.size()];
                    mCategoryIds = new Long[mCategoryList.size()];
                    mCategoriesImgs = new Bitmap[mCategoryList.size()];
                    int i = 0;
                    for (Categorias category : mCategoryList) {
                        mCategories[i] = category.getNome();
                        mCategoryIds[i] = category.getSubGrupoID();
                        if (category.getFoto() != null) {
                            try {
                                final byte[] decoded = Base64.decode(category.getFoto(), Base64.DEFAULT);
                                mCategoriesImgs[i] = BitmapFactory.decodeByteArray(decoded, 0, decoded.length);
                            } catch (IllegalArgumentException e) {
                                Log.e(TAG, e.getMessage(), e);
                                mCategoriesImgs[i] = BitmapFactory.decodeResource(getResources(),
                                        android.R.drawable.ic_menu_camera);
                            }

                        } else {
                            mCategoriesImgs[i] = null;
                        }
                        i++;
                    }
                    CategoryActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            // Adiciona as categorias na tela
                            RecyclerViewAdapter recyclerView_Adapter =
                                    new RecyclerViewAdapter(CategoryActivity.this, mCategories,
                                            mNumBarTab, mCategoryIds, mCategoriesImgs);
                            mRecyclerView.setAdapter(recyclerView_Adapter);
                        }
                    });

                }

            }
        };

        asyncTask.execute(TableInfo.CATEGORIES.name());
    }

}
