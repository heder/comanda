package upone.com.br.cluba;

import android.app.Application;
import android.content.Context;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;
import org.acra.config.ConfigurationBuilder;

/**
 * Created by Heder Guerreiro
 * on 28/05/2017.
 */
// suporte@up-one.com.br
@ReportsCrashes(mailTo = "suporte@up-one.com.br",
        mode = ReportingInteractionMode.DIALOG,
        customReportContent = {ReportField.APP_VERSION_CODE, ReportField.APP_VERSION_NAME,
                ReportField.ANDROID_VERSION, ReportField.PACKAGE_NAME, ReportField.PACKAGE_NAME,
                ReportField.REPORT_ID, ReportField.STACK_TRACE, ReportField.USER_APP_START_DATE,
                ReportField.USER_CRASH_DATE, ReportField.PHONE_MODEL, ReportField.CUSTOM_DATA,
                ReportField.STACK_TRACE, ReportField.LOGCAT})

public final class ClubaApplication extends Application {
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder(this);
        configurationBuilder.setResDialogText(R.string.crash_toast_text);
        configurationBuilder.setResDialogCommentPrompt(R.string.crash_dialog_comment_prompt);
        ACRA.init(this, configurationBuilder); // Descomente para gerar versao para entrega
    }
}
