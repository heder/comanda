package upone.com.br.cluba.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import upone.com.br.cluba.R;

import static upone.com.br.cluba.user.UserRecyclerViewAdapter.NUM_ID_BARTENDER;

public class BarcodeActivity extends AppCompatActivity {

    private String mNumIdBartender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode);
        if (getIntent() != null && getIntent().getExtras() != null) {
            mNumIdBartender = getIntent().getExtras().getString(NUM_ID_BARTENDER);
        }
        new IntentIntegrator(this).initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this, "Não foi possível capturar o valor desta comanda.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Comanda obtida: " + result.getContents(), Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra(MainActivity.NUM_BAR_TAB, result.getContents());
                intent.putExtra(NUM_ID_BARTENDER, mNumIdBartender);
                startActivity(intent);
            }
            finish();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
