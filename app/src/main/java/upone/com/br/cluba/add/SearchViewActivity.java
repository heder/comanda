package upone.com.br.cluba.add;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import io.swagger.client.ApiException;
import io.swagger.client.api.DefaultApi;
import io.swagger.client.model.Produtos;
import upone.com.br.cluba.R;
import upone.com.br.cluba.category.RecyclerViewAdapter;
import upone.com.br.cluba.database.ClubADB;
import upone.com.br.cluba.database.ConfigUtil;
import upone.com.br.cluba.database.TableInfo;
import upone.com.br.cluba.main.MainActivity;

public class SearchViewActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    public static final String PRODUCT_ID = "PRODUCT_ID";
    public static final int ACTION_QTY_COMP = 1;
    public static final String CATEGORY_ID = "CATEGORY_ID";
    public static final String PICTURE = "PICTURE";
    String[] mProfilePics;
    String[] mQuantitiesPrice;
    List<RowItem> mRowItems;
    ListView mListView;
    List<Produtos> mProductsList;
    CustomAdapter mAdapter;
    String[] mProductNames;
    private static final String TAG = SearchViewActivity.class.getName();
    private String mNumBarTab;
    private String mNumCategoryId;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_view);
        setTitle(getString(R.string.do_order));
        final Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mNumBarTab = extras.getString(MainActivity.NUM_BAR_TAB);
            mNumCategoryId = extras.getString(RecyclerViewAdapter.NUM_ID_CATEGORY);
        }
        getProductsByCategory();
    }

    @Override
    protected void onResume() {
        super.onResume();
        final Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mNumBarTab = extras.getString(MainActivity.NUM_BAR_TAB);
            mNumCategoryId = extras.getString(RecyclerViewAdapter.NUM_ID_CATEGORY);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_search, menu);
        MenuItem menuItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return true;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final Intent intent = new Intent(SearchViewActivity.this, QtyComplActivity.class);
        intent.putExtra(PRODUCT_ID, mProductsList.get(position).getProdutoID());
        intent.putExtra(MainActivity.NUM_BAR_TAB, mNumBarTab);
        intent.putExtra(CATEGORY_ID, mNumCategoryId);
        intent.putExtra(PICTURE, mProductsList.get(position).getFoto());
        startActivityForResult(intent, ACTION_QTY_COMP);
    }

    /**
     * Metodo que obtem as categorias
     */
    protected void getProductsByCategory() {
        final AsyncTask<String, Void, String> asyncTask = new AsyncTask<String, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(SearchViewActivity.this);
                mProgressDialog.setMessage(getString(R.string.searching));
                mProgressDialog.show();
            }

            @SuppressWarnings("unchecked")
            @Override
            protected String doInBackground(String... params) {
                final DefaultApi defaultApi = new DefaultApi();
                String response = null;
                try {
                    ConfigUtil.getInstance().setContext(SearchViewActivity.this);
                    defaultApi.setBasePath(ConfigUtil.getInstance().getBasePath() + ConfigUtil.getInstance().getApi());
                    mProductsList = defaultApi.getProdutos(Long.valueOf(mNumCategoryId));
                    response = "200";
                } catch (InterruptedException | ExecutionException | TimeoutException | ApiException e) {
                    Log.e(TAG, e.getMessage(), e);
                }
                return response;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if (mProgressDialog != null) {
                    mProgressDialog.dismiss();
                }
                if (mProductsList != null) {
                    mProductNames = new String[mProductsList.size()];
                    mQuantitiesPrice = new String[mProductsList.size()];
                    mProfilePics = new String[mProductsList.size()];
                    int i = 0;
                    for (Produtos product : mProductsList) {
                        mProductNames[i] = product.getNome();
                        mQuantitiesPrice[i] = "R$" + product.getValor();
                        mProfilePics[i] = product.getFoto();
                        i++;
                    }

                    // Adiciona os produtos na tela de busca
                    mRowItems = new ArrayList<>();
                    if (mProductNames.length == 0) {
                        Toast.makeText(SearchViewActivity.this, R.string.no_options_at_the_moment,
                                Toast.LENGTH_LONG).show();
                    } else {
                        for (i = 0; i < mProductNames.length; i++) {
                            RowItem item = new RowItem(mProductNames[i], mProfilePics[i],
                                    mQuantitiesPrice[i]);
                            mRowItems.add(item);
                        }

                        mListView = (ListView) findViewById(R.id.listViewResult);
                        mAdapter = new CustomAdapter(SearchViewActivity.this, R.layout.row_itens, mRowItems);
                        mListView.setAdapter(mAdapter);
                        mListView.setOnItemClickListener(SearchViewActivity.this);
                    }

                    ClubADB clubADB = new ClubADB(SearchViewActivity.this);
                    try {
                        // Limpa os produtos que estiverem previamente armazenados.
                        if (clubADB.getListObject(TableInfo.PRODUCTS.name(), Produtos.class) != null) {
                            clubADB.remove(TableInfo.PRODUCTS.name());
                        }
                        clubADB.putListObject(TableInfo.PRODUCTS.name(), (ArrayList<?>) mProductsList);
                    } catch (IOException e) {
                        Log.e(TAG, e.getMessage(), e);
                    }
                }
            }
        };

        asyncTask.execute(TableInfo.CATEGORIES.name());
    }

}
