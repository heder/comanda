package upone.com.br.cluba.category;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import upone.com.br.cluba.R;
import upone.com.br.cluba.add.SearchViewActivity;
import upone.com.br.cluba.main.MainActivity;

/**
 * UserRecyclerViewAdapter
 * <p>
 * Created by HEDER GUERREIRO on 27/03/2017.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    public static final String NUM_ID_CATEGORY = "NUM_ID_BARTENDER";
    private Context mContext;
    private String[] mValues;
    private String mNumBarTab;
    private Long[] mValuesCatIds;
    private Bitmap[] mBitmaps;

    RecyclerViewAdapter(CategoryActivity categoryActivity, String[] mCategories,
                        String mNumBarTab, Long[] mCategoryIds, Bitmap[] mCategoriesImgs) {
        this.mContext = categoryActivity;
        this.mValues = mCategories;
        this.mNumBarTab = mNumBarTab;
        this.mValuesCatIds = mCategoryIds;
        this.mBitmaps = mCategoriesImgs;
    }

    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view1 = LayoutInflater.from(mContext).inflate(R.layout.recycler_view_items, parent, false);
        return new ViewHolder(view1);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onBindViewHolder(final RecyclerViewAdapter.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final Bitmap bmp = Bitmap.createBitmap(mBitmaps[position]);
        holder.mImageView.setImageBitmap((Bitmap.createScaledBitmap(bmp, 80, 60, false)));
        holder.mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(mContext, SearchViewActivity.class);
                intent.addFlags(android.content.Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(MainActivity.NUM_BAR_TAB, mNumBarTab);
                intent.putExtra(NUM_ID_CATEGORY, String.valueOf(mValuesCatIds[position]));
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.length;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mImageView;

        ViewHolder(View itemView) {
            super(itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.imageView);
        }
    }
}