package upone.com.br.cluba.add;

/**
 * Row Item for products view search list.
 * <p>
 * Created by HEDER GUERREIRO on 24/03/2017.
 */

class RowItem {

    private String mProductDesc;
    private String mImagePic;
    private String mQty;
    private String mPrice;

    RowItem(String mProductDesc, String mImagePic, String mPrice) {
        this.mProductDesc = mProductDesc;
        this.mImagePic = mImagePic;
        this.mPrice = mPrice;
    }

    RowItem(String mProductDesc, String mImagePic, String mQty, String mPrice) {
        this.mProductDesc = mProductDesc;
        this.mImagePic = mImagePic;
        this.mQty = mQty;
        this.mPrice = mPrice;
    }

    String getmProductDesc() {
        return mProductDesc;
    }

    String getmImagePic() {
        return mImagePic;
    }

    String getmQty() {
        return mQty;
    }

    String getmPrice() {
        return mPrice;
    }
}
