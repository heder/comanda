package upone.com.br.cluba.database;

import android.annotation.SuppressLint;
import android.content.Context;

/**
 * ConfigUtil class.
 * <p>
 * Created by Heder Guerreiro on 05/05/2017.
 */

public class ConfigUtil {

    public static final boolean IS_LOGGING_ENABLED = false;
    public static final String EMAIL_TO_LOG_SENDING = "suporte@up-one.com.br";
    @SuppressWarnings("FieldCanBeLocal")
    private final String PASSWORD = "Up0n3!@";
    // http://192.168.25.10:10601 interno |http://201.74.174.6:10601 externo
    // | http://webapi2.up-one.com.br http://clubinfinity.ddns.net:10601
    private String basePath = "http://192.168.25.10:10601";
    // /api
    private String api = "/api";
    private Context context;

    @SuppressLint("StaticFieldLeak")
    private static final ConfigUtil ourInstance = new ConfigUtil();

    public static ConfigUtil getInstance() {
        return ourInstance;
    }

    private ConfigUtil() {
    }

    public String getBasePath() {
        final ClubADB clubADB = new ClubADB(context);
        final String bp = clubADB.getString("BASEPATH");
        if (!bp.isEmpty()) {
            basePath = bp;
        }
        return basePath;
    }

    public void setBasePath(String basePath) {
        final ClubADB clubADB = new ClubADB(context);
        clubADB.putString("BASEPATH", basePath);
        this.basePath = basePath;
    }

    public String getApi() {
        final ClubADB clubADB = new ClubADB(context);
        final String ap = clubADB.getString("API");
        if (!ap.isEmpty()) {
            this.api = ap;
        }
        return this.api;
    }

    public void setApi(String api) {
        final ClubADB clubADB = new ClubADB(context);
        clubADB.putString("API", api);
        this.api = api;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public boolean validatePassword(String password) {
        return password.equals(PASSWORD);
    }
}
