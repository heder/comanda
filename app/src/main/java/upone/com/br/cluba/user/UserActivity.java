package upone.com.br.cluba.user;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import io.swagger.client.ApiException;
import io.swagger.client.api.DefaultApi;
import io.swagger.client.model.Garcom;
import upone.com.br.cluba.R;
import upone.com.br.cluba.database.ClubADB;
import upone.com.br.cluba.database.ConfigUtil;
import upone.com.br.cluba.database.TableInfo;


public class UserActivity extends AppCompatActivity {

    private static final String TAG = UserActivity.class.getName();
    private static final int NUMBER_COLUMNS = 2;
    private List<Garcom> mBartendersList;
    private Long[] mBartendersIds;
    private ProgressDialog mProgressDialog;
    private RecyclerView mRecyclerView;
    private String[] mBartNames;
    private ClubADB mClubADB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        setTitle(getString(R.string.bartender) + " - (" + Build.VERSION.RELEASE + ")");

        mClubADB = new ClubADB(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setup();
    }

    /**
     * Limpa o db.
     */
    private void cleanDB() {
        mClubADB.remove(TableInfo.PRODUCTS.name());
        mClubADB.remove(TableInfo.ITEMS_COMANDA.name());
        mClubADB.remove(TableInfo.CATEGORIES.name());
        mClubADB.remove(TableInfo.PRODUCTS_BAR_TAB.name());
        mClubADB.remove(TableInfo.ITEMS_COMANDA_LIST.name());
        mClubADB.remove(TableInfo.BARTENDERS.name());
        if (!ConfigUtil.IS_LOGGING_ENABLED) {
            mClubADB.remove(TableInfo.JSON_LOG.name());
        }
    }

    /**
     * Metodo de setup
     */
    protected void setup() {
        // Limpa a tabela.
        cleanDB();
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view1);
        GridLayoutManager mRecyclerViewLayoutManager = new GridLayoutManager(UserActivity.this, NUMBER_COLUMNS);
        mRecyclerView.setLayoutManager(mRecyclerViewLayoutManager);
        getGarcoms();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if (ConfigUtil.IS_LOGGING_ENABLED) {
            inflater.inflate(R.menu.about_3, menu);
        } else {
            inflater.inflate(R.menu.about_2, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @SuppressLint("InflateParams")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final LayoutInflater inflater = getLayoutInflater();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final View inflate = inflater.inflate(R.layout.dialog_signin, null);
        final View inflate1 = inflater.inflate(R.layout.dialog_config_url, null);
        switch (item.getItemId()) {
            case R.id.admin:
                builder.setView(inflate);
                builder.setCancelable(true);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditText password = (EditText) inflate.findViewById(R.id.password);
                        if (password.getText() == null) {
                            Toast.makeText(UserActivity.this, getString(R.string.info_senha), Toast.LENGTH_SHORT).show();
                        } else if (!ConfigUtil.getInstance().validatePassword(password.getText().toString())) {
                            Toast.makeText(UserActivity.this, getString(R.string.admin_sistema), Toast.LENGTH_SHORT).show();
                        } else {
                            dialog.dismiss();
                            AlertDialog.Builder builder = new AlertDialog.Builder(UserActivity.this);
                            builder.setView(inflate1);
                            builder.setCancelable(true);
                            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    EditText server = (EditText) inflate1.findViewById(R.id.server);
                                    EditText api = (EditText) inflate1.findViewById(R.id.api);
                                    if (server.getText().length() == 0) {
                                        Toast.makeText(UserActivity.this, getString(R.string.link_to_server),
                                                Toast.LENGTH_SHORT).show();
                                    }
                                    if (api.getText().length() == 0) {
                                        Toast.makeText(UserActivity.this, getString(R.string.link_to_api),
                                                Toast.LENGTH_SHORT).show();
                                    } else {
                                        ConfigUtil.getInstance().setContext(UserActivity.this);
                                        ConfigUtil.getInstance().setBasePath(String.valueOf(server.getText()));
                                        ConfigUtil.getInstance().setApi(String.valueOf(api.getText()));
                                    }
                                }
                            });
                            final AlertDialog dialodConfig = builder.create();
                            dialodConfig.show();
                            final Button positiveButton = dialodConfig.getButton(AlertDialog.BUTTON_POSITIVE);
                            LinearLayout.LayoutParams positiveButtonLL = (LinearLayout.LayoutParams) positiveButton.getLayoutParams();
                            positiveButtonLL.gravity = Gravity.CENTER;
                            positiveButton.setLayoutParams(positiveButtonLL);
                        }
                        dialog.cancel();
                    }
                });
                final AlertDialog dialog = builder.create();
                dialog.show();
                final Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                LinearLayout.LayoutParams positiveButtonLL = (LinearLayout.LayoutParams) positiveButton.getLayoutParams();
                positiveButtonLL.gravity = Gravity.CENTER;
                positiveButton.setLayoutParams(positiveButtonLL);
                break;
            case R.id.log:
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{ConfigUtil.EMAIL_TO_LOG_SENDING});
                @SuppressLint("SimpleDateFormat") final SimpleDateFormat simpleDateFormat =
                        new SimpleDateFormat("dd-MM-yyyy_HH:mm:ss");
                final String date = simpleDateFormat.format(Calendar.getInstance().getTime());
                i.putExtra(Intent.EXTRA_SUBJECT, "COMANDA LOG "
                        + date);
                final ArrayList<String> listString = mClubADB.getListString(TableInfo.JSON_LOG.name());
                String str = " *** ";
                if (!listString.isEmpty()) {
                    for (String s : listString) {
                        str = str + s + " *** ";
                    }
                }
                i.putExtra(Intent.EXTRA_TEXT, str);
                try {
                    startActivity(Intent.createChooser(i, "Enviar mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(UserActivity.this, "Não há aplicativo de email instalado.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Metodo que obtem os garcoms
     */
    protected void getGarcoms() {
        final AsyncTask<String, Void, String> asyncTask = new AsyncTask<String, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(UserActivity.this);
                mProgressDialog.setMessage(getString(R.string.searching));
                mProgressDialog.show();
            }

            @SuppressWarnings("unchecked")
            @Override
            protected String doInBackground(String... params) {
                final DefaultApi defaultApi = new DefaultApi();
                String response = null;
                try {
                    ConfigUtil.getInstance().setContext(UserActivity.this);
                    defaultApi.setBasePath(ConfigUtil.getInstance().getBasePath()
                            + ConfigUtil.getInstance().getApi());
                    mBartendersList = defaultApi.getGarcoms();
                    if (mBartendersList != null && !mBartendersList
                            .isEmpty()) {
                        mClubADB.putListObject(TableInfo.BARTENDERS.name(), (ArrayList<?>) mBartendersList);
                        response = "200";
                    } else {
                        new AlertDialog.Builder(UserActivity.this).
                                setMessage(R.string.garcom_message).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();
                    }
                } catch (TimeoutException | ExecutionException | ApiException | InterruptedException e) {
                    Log.e(TAG, e.getMessage(), e);
                }
                return response;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if (mProgressDialog != null) {
                    mProgressDialog.dismiss();
                }
                if (mBartendersList != null) {
                    mBartNames = new String[mBartendersList.size()];
                    mBartendersIds = new Long[mBartendersList.size()];
                    int i = 0;
                    for (Garcom bartender : mBartendersList) {
                        mBartNames[i] = bartender.getNome();
                        mBartendersIds[i] = bartender.getPessoaID();
                        i++;
                    }
                    UserActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            // Adiciona os garcoms na tela
                            UserRecyclerViewAdapter recyclerView_Adapter =
                                    new UserRecyclerViewAdapter(UserActivity.this, mBartNames,
                                            mBartendersIds);
                            mRecyclerView.setAdapter(recyclerView_Adapter);
                        }
                    });

                }

            }
        };

        asyncTask.execute(TableInfo.CATEGORIES.name());
    }

}
