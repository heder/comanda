package upone.com.br.cluba.main;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import io.swagger.client.ApiException;
import io.swagger.client.api.DefaultApi;
import io.swagger.client.model.ItemsComanda;
import upone.com.br.cluba.R;
import upone.com.br.cluba.add.BarTabListActivity;
import upone.com.br.cluba.category.CategoryActivity;
import upone.com.br.cluba.database.ClubADB;
import upone.com.br.cluba.database.ConfigUtil;
import upone.com.br.cluba.database.TableInfo;
import upone.com.br.cluba.user.UserActivity;

import static upone.com.br.cluba.user.UserRecyclerViewAdapter.NUM_ID_BARTENDER;

/**
 * Tela principal.
 * <p>
 * Created by HEDER GUERREIRO on 25/03/2017.
 */
public class MainActivity extends AppCompatActivity {

    /**
     * Atributos estaticos.
     */
    private static final String TAG = MainActivity.class.getName();
    public static String NUM_BAR_TAB = "NUM_BAR_TAB";

    /**
     * Atributos de tela.
     */
    private String mNumBarTabValue;
    private String mBartenderId;
    private TextView mTextView;
    private Button mButton;
    private Button mButton2;
    private EditText mNumBarTab;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (getIntent() != null && getIntent().getExtras() != null) {
            mNumBarTabValue = getIntent().getExtras().getString(NUM_BAR_TAB);
            mBartenderId = getIntent().getExtras().getString(NUM_ID_BARTENDER);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setup();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), UserActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    /**
     * Setup
     */
    private void setup() {

        // Components
        if (mTextView == null) {
            mTextView = (TextView) findViewById(R.id.inf_name_tv);
            mTextView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Raleway-SemiBoldItalic.ttf"));
        }

        if (mNumBarTab == null) {
            mNumBarTab = (EditText) findViewById(R.id.num_bar_tab_et);
            mNumBarTab.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Raleway-Regular.ttf"));
            if (mNumBarTabValue != null && mNumBarTabValue.length() > 0 && !mNumBarTabValue.equals("null")) {
                mNumBarTab.setText(mNumBarTabValue);
            }
            mNumBarTab.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        validateBarTab();
                    }
                    return true;
                }
            });
        }

        if (mButton == null) {
            mButton = (Button) findViewById(R.id.ok_bt);
            mButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    validateBarTab();
                }
            });
        }

        if (mButton2 == null) {
            mButton2 = (Button) findViewById(R.id.cod_btn);
            mButton2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this, BarcodeActivity.class);
                    intent.putExtra(NUM_ID_BARTENDER, mBartenderId);
                    startActivityForResult(intent, 1);
                }
            });
        }

    }

    /**
     * Inicia a activity de categorias.
     */
    private void startCategories() {
        final Intent intent = new Intent(this, CategoryActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(NUM_BAR_TAB, String.valueOf(mNumBarTab.getText()));
        saveBarTab();
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.about, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @SuppressLint("InflateParams")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final LayoutInflater inflater = getLayoutInflater();
        AlertDialog.Builder builder;
        final View inflate2 = inflater.inflate(R.layout.dialog_list, null);
        switch (item.getItemId()) {
            case R.id.list:
                if (mNumBarTab.getText() != null && !mNumBarTab.getText().toString().isEmpty()) {
                    final Intent intent = new Intent(MainActivity.this, BarTabListActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(NUM_BAR_TAB, mNumBarTab.getText().toString());
                    getApplicationContext().startActivity(intent);
                } else {
                    builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setView(inflate2);
                    builder.setCancelable(true);
                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            EditText list = (EditText) inflate2.findViewById(R.id.num_bar_tab_et);
                            if (list.getText().length() == 0) {
                                dialog.dismiss();
                                Toast.makeText(MainActivity.this, getString(R.string.informe_o_n_mero_da_comanda),
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                mNumBarTab.setText(list.getText());
                                dialog.dismiss();
                                final Intent intent = new Intent(MainActivity.this, BarTabListActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra(NUM_BAR_TAB, mNumBarTab.getText().toString());
                                getApplicationContext().startActivity(intent);
                            }
                        }
                    });
                    final AlertDialog dialodConfig = builder.create();
                    dialodConfig.show();

                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Salva a comanda.
     */
    @SuppressLint("HardwareIds")
    private void saveBarTab() {
        ItemsComanda itemsComanda = new ItemsComanda();
        itemsComanda.setComandaID(Long.valueOf(mNumBarTab.getText().toString()));
        itemsComanda.setUsuario(mBartenderId);
        final String androidId = System.getProperty(Settings.Secure.ANDROID_ID);
        itemsComanda.setEstacaoTrabalho(androidId == null ? "0000" : androidId);
        final ClubADB clubADB = new ClubADB(this);
        clubADB.putObject(TableInfo.ITEMS_COMANDA.name(), itemsComanda);
    }

    /**
     * Valida os dados.
     */
    private boolean validateBarTab() {
        if (mNumBarTab.getText().toString().length() == 0) {
            mNumBarTab.setError("Informe o número da comanda");
            return false;
        }
        return liberateBarTab();
    }

    /**
     * Valida se a comanda esta liberada ou nao.
     *
     * @return true se eh valida e false se for o contrario
     */
    private boolean liberateBarTab() {
        final boolean[] result = {false};
        final Long value = Long.valueOf(mNumBarTab.getText().toString());
        final AsyncTask<String, Void, String> asyncTask = new AsyncTask<String, Void, String>() {

            @Override
            protected void onPreExecute() {
                mProgressDialog = new ProgressDialog(MainActivity.this);
                mProgressDialog.setMessage(getString(R.string.validating_bar_tab));
                mProgressDialog.show();
                super.onPreExecute();
            }

            @SuppressWarnings("unchecked")
            @Override
            protected String doInBackground(String... params) {
                String response = null;
                final DefaultApi defaultApi = new DefaultApi();
                try {
                    ConfigUtil.getInstance().setContext(MainActivity.this);
                    defaultApi.setBasePath(ConfigUtil.getInstance().getBasePath()
                            + ConfigUtil.getInstance().getApi());
                    response = defaultApi.getComanda(value);
                    final String pattern = "ComandaID";
                    if (!response.contains(pattern)) {
                        final String finalResponse = response;
                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, finalResponse, Toast.LENGTH_SHORT).show();
                                mNumBarTab.setText("");
                            }
                        });
                        result[0] = false;
                    } else {
                        result[0] = true;
                        startCategories();
                    }
                } catch (TimeoutException | ExecutionException | ApiException | InterruptedException e) {
                    Log.e(TAG, e.getMessage(), e);
                }
                return response;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                mNumBarTabValue = extractNumber(s);
                if (mProgressDialog != null) {
                    mProgressDialog.dismiss();
                }

            }
        };

        asyncTask.execute(TableInfo.ITEMS_COMANDA.name());
        return result[0];
    }

    /**
     * Extrai ID da comanda.
     *
     * @param value valor de resposta do servico
     * @return Id da comanda
     */
    private String extractNumber(String value) {
        if (value != null) {
            value = value.replaceAll("\\D+", "");
            value = value.trim();
        }
        return value;
    }

}
