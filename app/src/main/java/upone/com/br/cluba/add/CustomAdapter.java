package upone.com.br.cluba.add;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import upone.com.br.cluba.R;

/**
 * Custom Adapter para lista de itens da comanda.
 * <p>
 * Created by HEDER GUERREIRO on 24/03/2017.
 */

class CustomAdapter extends BaseAdapter implements Filterable {

    private Context mContext;
    private List<RowItem> mRowItens;
    private List<RowItem> mRowItensFiltered;
    private LayoutInflater mInflater;
    private int mResource;
    private ItemFilter mFilter;

    CustomAdapter(Context mContext, int mResource, List<RowItem> mRowItens) {
        super();
        this.mContext = mContext;
        this.mResource = mResource;
        this.mRowItens = mRowItens;
        this.mRowItensFiltered = mRowItens;
        mInflater = LayoutInflater.from(mContext);
        mFilter = new ItemFilter();
    }

    @Override
    public int getCount() {
        return mRowItensFiltered.size();
    }

    @Override
    public Object getItem(int position) {
        return mRowItensFiltered.get(position);
    }

    @SuppressWarnings("SuspiciousMethodCalls")
    @Override
    public long getItemId(int position) {
        return mRowItensFiltered.indexOf(getItem(position));
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = new ViewHolder();
        if (convertView == null) {
            convertView = mInflater.inflate(mResource, null);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            if (viewHolder == null) {
                viewHolder = new ViewHolder();
            }
        }
        viewHolder.mProductTV = (TextView) convertView.findViewById(R.id.productTV);
        if (mResource != R.layout.row_itens_3) {
            viewHolder.mProfilePicIV = (ImageView) convertView.findViewById(R.id.profile_picIV);
        }
        viewHolder.mPriceTV = (TextView) convertView.findViewById(R.id.priceTV);
        viewHolder.mPriceTV.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "fonts/Raleway-SemiBold.ttf"));
        if (mResource == R.layout.row_itens_2 || mResource == R.layout.row_itens_3) {
            viewHolder.mQtyTV = (TextView) convertView.findViewById(R.id.qtyTV);
            viewHolder.mQtyTV.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "fonts/Raleway-Black.ttf"));
        }
        RowItem rowPos = mRowItensFiltered.get(position);
        if (rowPos != null) {
            if (mResource != R.layout.row_itens_3) {
                Bitmap bmp;
                try {
                    final byte[] decoded = Base64.decode(rowPos.getmImagePic(), Base64.DEFAULT);
                    bmp = BitmapFactory.decodeByteArray(decoded, 0, decoded.length);
                } catch (NullPointerException | IllegalArgumentException e) {
                    Log.e(CustomAdapter.class.getName(), e.getMessage(), e);
                    bmp = BitmapFactory.decodeResource(mContext.getResources(),
                            android.R.drawable.ic_menu_camera);
                }
                viewHolder.mProfilePicIV.setImageBitmap((Bitmap.createScaledBitmap(bmp, 80, 60, false)));
            }
            viewHolder.mProductTV.setText(rowPos.getmProductDesc());
            viewHolder.mPriceTV.setText(rowPos.getmPrice());
            if (mResource == R.layout.row_itens_2 || mResource == R.layout.row_itens_3) {
                viewHolder.mQtyTV.setText(rowPos.getmQty());
            }
        }
        return convertView;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            final String filterString = constraint.toString().toLowerCase();
            final FilterResults filterResults = new FilterResults();
            final List<RowItem> rowItems = mRowItens;
            final int count = rowItems.size();
            final List<RowItem> filteredList = new ArrayList<>(count);
            for (int i = 0; i < count; i++) {
                RowItem rowItem = rowItems.get(i);
                if (rowItem.getmProductDesc().toLowerCase().contains(filterString)) {
                    filteredList.add(rowItem);
                }
            }

            filterResults.values = filteredList;
            filterResults.count = filteredList.size();
            return filterResults;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mRowItensFiltered = (ArrayList<RowItem>) results.values;
            if (mRowItensFiltered.size() == 0) {
                notifyDataSetInvalidated();
            } else {
                notifyDataSetChanged();
            }

        }
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private class ViewHolder {
        ImageView mProfilePicIV;
        TextView mProductTV;
        TextView mPriceTV;
        TextView mQtyTV;
    }

}
