package upone.com.br.cluba.database;

/**
 * Created by Heder Guerreiro
 * on 31/03/2017.
 */

public enum TableInfo {
    PRODUCTS, ITEMS_COMANDA, ITEMS_COMANDA_LIST, PRODUCTS_BAR_TAB, CATEGORIES, BARTENDERS, JSON_LOG
}
