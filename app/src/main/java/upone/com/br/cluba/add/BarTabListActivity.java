package upone.com.br.cluba.add;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import io.swagger.client.ApiException;
import io.swagger.client.JsonUtil;
import io.swagger.client.api.DefaultApi;
import io.swagger.client.model.ItemsComanda;
import io.swagger.client.model.Produtos;
import upone.com.br.cluba.R;
import upone.com.br.cluba.database.ClubADB;
import upone.com.br.cluba.database.ConfigUtil;
import upone.com.br.cluba.database.TableInfo;
import upone.com.br.cluba.main.MainActivity;

public class BarTabListActivity extends AppCompatActivity {

    /**
     * Static attributes
     */
    private static final String TAG = "AddActivity";

    // private static final
    /**
     * Screen attributes
     */
    private String mNumBarTab;
    private FloatingActionButton closeFAB;
    private ItemsComanda mBarTab;
    private List<Produtos> mProductsList;
    private ClubADB mClubADB;
    private ArrayList<ItemsComanda> mItemsComandas;
    private String[] mProfilePics;
    private ProgressDialog mProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar_tab_list);

        //Getting bundle values
        final Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mNumBarTab = extras.getString(MainActivity.NUM_BAR_TAB);
        }
        setup();
        addBarTabAtList();
    }

    /**
     * Setup
     */
    private void setup() {

        // O titulo eh o numero da comanda.
        mNumBarTab = getIntent().getExtras().getString(MainActivity.NUM_BAR_TAB);
        setTitle("Histórico de Consumo");

        if (closeFAB == null) {
            closeFAB = (FloatingActionButton) findViewById(R.id.fabClose);
            closeFAB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }

    }

    /**
     * Carrega os itens de comanda para a lista de produtos. Caso exista itens de comanda.
     */
    protected void addBarTabAtList() {
        final AsyncTask<String, Void, String> asyncTask = new AsyncTask<String, Void, String>() {

            @Override
            protected void onPreExecute() {
                mProgressDialog = new ProgressDialog(BarTabListActivity.this);
                mProgressDialog.setMessage(getString(R.string.searching));
                mProgressDialog.show();
                super.onPreExecute();
            }

            @SuppressWarnings("unchecked")
            @Override
            protected String doInBackground(String... params) {
                try {
                    DefaultApi defaultApi = new DefaultApi();
                    ConfigUtil.getInstance().setContext(BarTabListActivity.this);
                    defaultApi.setBasePath(ConfigUtil.getInstance().getBasePath() + ConfigUtil.getInstance().getApi());
                    String response = defaultApi.getItensComanda(Long.valueOf(mNumBarTab));
                    if (response != null) {
                        mItemsComandas = JsonUtil.deserializeToList(response, ItemsComanda.class);
                        mBarTab = (ItemsComanda) getmClubADB().getObject(TableInfo.ITEMS_COMANDA.name(), ItemsComanda.class);
                    }
                    if (mProductsList == null) {
                        mProductsList = new ArrayList<>();
                    }
                    if (mItemsComandas != null) {
                        for (ItemsComanda itemComanda : mItemsComandas) {
                            Produtos produto = new Produtos();
                            produto.setProdutoID(itemComanda.getProdutoID());
                            produto.setNome(itemComanda.getDescricao());
                            produto.setValor(itemComanda.getValorProduto());
                            mProductsList.add(produto);
                        }
                    } else {
                        // nao há itens pra adicionar
                        mItemsComandas = new ArrayList<>(mProductsList.size());
                    }
                } catch (IOException | InterruptedException | ExecutionException | TimeoutException | ApiException e1) {
                    Log.e(TAG, e1.getMessage(), e1);
                }
                return String.valueOf(mBarTab);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                fillList();
                if (mProgressDialog != null) {
                    mProgressDialog.dismiss();
                }
                if (mProductsList.isEmpty()) {
                    Toast.makeText(BarTabListActivity.this, R.string.no_itens_consumed, Toast.LENGTH_SHORT).show();
                }
            }
        };

        asyncTask.execute(TableInfo.ITEMS_COMANDA.name());
    }

    private void fillList() {
        if (mProductsList != null) {
            String[] mProductNames = new String[mProductsList.size()];
            String[] mQuantities = new String[mProductsList.size()];
            mProfilePics = new String[mProductsList.size()];
            String[] mPrizes = new String[mProductsList.size()];
            int i = 0;
            for (Produtos product : mProductsList) {
                mProductNames[i] = product.getNome();
                mQuantities[i] = String.valueOf(mItemsComandas.get(i).getQuantidade());
                mPrizes[i] = String.valueOf(mItemsComandas.get(i).getValorProduto());
                mProfilePics[i] = product.getFoto();
                i++;
            }

            // Adiciona os produtos na tela de busca
            ArrayList<RowItem> mRowItems = new ArrayList<>();
            for (i = 0; i < mProductNames.length; i++) {
                RowItem item = new RowItem(mProductNames[i],
                        mProfilePics[i], mQuantities[i], "R$" + mPrizes[i]);
                mRowItems.add(item);
            }

            ListView mListView = (ListView) findViewById(R.id.listViewResult);
            CustomAdapter mAdapter = new CustomAdapter(BarTabListActivity.this, R.layout.row_itens_3, mRowItems);
            mListView.setAdapter(mAdapter);
        }
    }

    /**
     * Retorna uma instancia do banco
     *
     * @return ClubADB
     */
    private ClubADB getmClubADB() {
        if (mClubADB == null) {
            mClubADB = new ClubADB(BarTabListActivity.this);
        }
        return mClubADB;
    }
}
