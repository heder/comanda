package upone.com.br.cluba.user;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import upone.com.br.cluba.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.pressImeActionButton;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@SuppressWarnings("deprecation")
@LargeTest
@RunWith(AndroidJUnit4.class)
public class UserActivityTest2 {

    private static final int COUNT_TO_TEST = 10;

    @Rule
    public ActivityTestRule<UserActivity> mActivityTestRule = new ActivityTestRule<>(UserActivity.class);

    @Test
    public void userActivityTest2() {
        ViewInteraction appCompatTextView = onView(
                allOf(withId(R.id.textView), withText("Bina"), withContentDescription("Garçom"),
                        withParent(allOf(withId(R.id.cardview1),
                                withParent(withId(R.id.recycler_view1)))),
                        isDisplayed()));
        appCompatTextView.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.num_bar_tab_et), isDisplayed()));
        appCompatEditText.perform(click());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.num_bar_tab_et), isDisplayed()));
        appCompatEditText2.perform(replaceText("100"), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.num_bar_tab_et), withText("100"), isDisplayed()));
        appCompatEditText3.perform(pressImeActionButton());

        ViewInteraction appCompatImageView = onView(
                allOf(withIndex(withId(R.id.imageView), 0), withContentDescription("Categoria"),
                        withParent(allOf(withId(R.id.cardview1),
                                withParent(withId(R.id.recycler_view1)))),
                        isDisplayed()));
        appCompatImageView.perform(click());

        ViewInteraction relativeLayout = onView(
                allOf(childAtPosition(
                        withId(R.id.listViewResult),
                        3),
                        isDisplayed()));
        relativeLayout.perform(click());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.rightButton), isDisplayed()));
        appCompatButton.perform(click());

        ViewInteraction floatingActionButton = onView(
                allOf(withId(R.id.fabClose), isDisplayed()));
        floatingActionButton.perform(click());

        ViewInteraction floatingActionButton2 = onView(
                allOf(withClassName(is("com.github.clans.fab.FloatingActionButton")),
                        withParent(withId(R.id.menu_yellow)),
                        isDisplayed()));
        floatingActionButton2.perform(click());

        ViewInteraction floatingActionButton3 = onView(
                allOf(withId(R.id.fabNew),
                        withParent(withId(R.id.menu_yellow)),
                        isDisplayed()));
        floatingActionButton3.perform(click());

        ViewInteraction appCompatImageView2 = onView(
                allOf(withIndex(withId(R.id.imageView), 3), withContentDescription("Categoria"),
                        withParent(allOf(withId(R.id.cardview1),
                                withParent(withId(R.id.recycler_view1)))),
                        isDisplayed()));
        appCompatImageView2.perform(click());

        ViewInteraction relativeLayout2 = onView(
                allOf(childAtPosition(
                        withId(R.id.listViewResult),
                        3),
                        isDisplayed()));
        relativeLayout2.perform(click());

        ViewInteraction appCompatButton2 = onView(
                allOf(withId(R.id.rightButton), isDisplayed()));
        appCompatButton2.perform(click());

        ViewInteraction floatingActionButton4 = onView(
                allOf(withId(R.id.fabClose), isDisplayed()));
        floatingActionButton4.perform(click());

        ViewInteraction floatingActionButton5 = onView(
                allOf(withClassName(is("com.github.clans.fab.FloatingActionButton")),
                        withParent(withId(R.id.menu_yellow)),
                        isDisplayed()));
        floatingActionButton5.perform(click());

        ViewInteraction floatingActionButton6 = onView(
                allOf(withId(R.id.fabNew),
                        withParent(withId(R.id.menu_yellow)),
                        isDisplayed()));
        floatingActionButton6.perform(click());

        ViewInteraction appCompatImageView3 = onView(
                allOf(withIndex(withId(R.id.imageView), 4), withContentDescription("Categoria"),
                        withParent(allOf(withId(R.id.cardview1),
                                withParent(withId(R.id.recycler_view1)))),
                        isDisplayed()));
        appCompatImageView3.perform(click());

        ViewInteraction relativeLayout3 = onView(
                allOf(childAtPosition(
                        withId(R.id.listViewResult),
                        3),
                        isDisplayed()));
        relativeLayout3.perform(click());

        ViewInteraction appCompatButton3 = onView(
                allOf(withId(R.id.rightButton), isDisplayed()));
        appCompatButton3.perform(click());

        ViewInteraction appCompatButton4 = onView(
                allOf(withId(R.id.rightButton), isDisplayed()));
        appCompatButton4.perform(click());

        ViewInteraction appCompatButton5 = onView(
                allOf(withId(R.id.rightButton), isDisplayed()));
        appCompatButton5.perform(click());

        ViewInteraction floatingActionButton7 = onView(
                allOf(withId(R.id.fabClose), isDisplayed()));
        floatingActionButton7.perform(click());

        ViewInteraction floatingActionButton8 = onView(
                allOf(withClassName(is("com.github.clans.fab.FloatingActionButton")),
                        withParent(withId(R.id.menu_yellow)),
                        isDisplayed()));
        floatingActionButton8.perform(click());

        ViewInteraction floatingActionButton9 = onView(
                allOf(withId(R.id.fabClose),
                        withParent(withId(R.id.menu_yellow)),
                        isDisplayed()));
        floatingActionButton9.perform(click());

        ViewInteraction appCompatButton7 = onView(
                allOf(withId(android.R.id.button1), withText("OK"),
                        withParent(allOf(withClassName(is("android.widget.LinearLayout")),
                                withParent(withClassName(is("android.widget.LinearLayout"))))),
                        isDisplayed()));
        appCompatButton7.perform(click());

        ViewInteraction appCompatTextView2 = onView(
                allOf(withId(R.id.textView), withText("Thomy"), withContentDescription("Garçom"),
                        withParent(allOf(withId(R.id.cardview1),
                                withParent(withId(R.id.recycler_view1)))),
                        isDisplayed()));
        appCompatTextView2.perform(click());

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.num_bar_tab_et), isDisplayed()));
        appCompatEditText4.perform(click());

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.num_bar_tab_et), isDisplayed()));
        appCompatEditText5.perform(replaceText("100"), closeSoftKeyboard());

        ViewInteraction appCompatEditText6 = onView(
                allOf(withId(R.id.num_bar_tab_et), withText("100"), isDisplayed()));
        appCompatEditText6.perform(pressImeActionButton());

        ViewInteraction appCompatImageView4 = onView(
                allOf(withIndex(withId(R.id.imageView), 6), withContentDescription("Categoria"),
                        withParent(allOf(withId(R.id.cardview1),
                                withParent(withId(R.id.recycler_view1)))),
                        isDisplayed()));
        appCompatImageView4.perform(click());

        ViewInteraction relativeLayout4 = onView(
                allOf(childAtPosition(
                        withId(R.id.listViewResult),
                        1),
                        isDisplayed()));
        relativeLayout4.perform(click());

        ViewInteraction appCompatButton8 = onView(
                allOf(withId(R.id.rightButton), isDisplayed()));
        appCompatButton8.perform(click());

        ViewInteraction floatingActionButton10 = onView(
                allOf(withId(R.id.fabClose), isDisplayed()));
        floatingActionButton10.perform(click());

        ViewInteraction floatingActionButton11 = onView(
                allOf(withClassName(is("com.github.clans.fab.FloatingActionButton")),
                        withParent(withId(R.id.menu_yellow)),
                        isDisplayed()));
        floatingActionButton11.perform(click());

        ViewInteraction floatingActionButton12 = onView(
                allOf(withId(R.id.fabNew),
                        withParent(withId(R.id.menu_yellow)),
                        isDisplayed()));
        floatingActionButton12.perform(click());

        ViewInteraction appCompatImageView5 = onView(
                allOf(withIndex(withId(R.id.imageView), 8), withContentDescription("Categoria"),
                        withParent(allOf(withId(R.id.cardview1),
                                withParent(withId(R.id.recycler_view1)))),
                        isDisplayed()));
        appCompatImageView5.perform(click());

        ViewInteraction relativeLayout5 = onView(
                allOf(childAtPosition(
                        withId(R.id.listViewResult),
                        0),
                        isDisplayed()));
        relativeLayout5.perform(click());

        ViewInteraction floatingActionButton13 = onView(
                allOf(withId(R.id.fabClose), isDisplayed()));
        floatingActionButton13.perform(click());

        ViewInteraction floatingActionButton14 = onView(
                allOf(withClassName(is("com.github.clans.fab.FloatingActionButton")),
                        withParent(withId(R.id.menu_yellow)),
                        isDisplayed()));
        floatingActionButton14.perform(click());

        ViewInteraction floatingActionButton15 = onView(
                allOf(withId(R.id.fabNew),
                        withParent(withId(R.id.menu_yellow)),
                        isDisplayed()));
        floatingActionButton15.perform(click());

        ViewInteraction appCompatImageView6 = onView(
                allOf(withIndex(withId(R.id.imageView), 4), withContentDescription("Categoria"),
                        withParent(allOf(withId(R.id.cardview1),
                                withParent(withId(R.id.recycler_view1)))),
                        isDisplayed()));
        appCompatImageView6.perform(click());

        ViewInteraction relativeLayout6 = onView(
                allOf(childAtPosition(
                        withId(R.id.listViewResult),
                        2),
                        isDisplayed()));
        relativeLayout6.perform(click());

        ViewInteraction appCompatButton9 = onView(
                allOf(withId(R.id.rightButton), isDisplayed()));
        appCompatButton9.perform(click());

        ViewInteraction appCompatButton10 = onView(
                allOf(withId(R.id.rightButton), isDisplayed()));
        appCompatButton10.perform(click());

        ViewInteraction floatingActionButton16 = onView(
                allOf(withId(R.id.fabClose), isDisplayed()));
        floatingActionButton16.perform(click());

        ViewInteraction floatingActionButton17 = onView(
                allOf(withClassName(is("com.github.clans.fab.FloatingActionButton")),
                        withParent(withId(R.id.menu_yellow)),
                        isDisplayed()));
        floatingActionButton17.perform(click());

        ViewInteraction floatingActionButton18 = onView(
                allOf(withId(R.id.fabNew),
                        withParent(withId(R.id.menu_yellow)),
                        isDisplayed()));
        floatingActionButton18.perform(click());

        ViewInteraction appCompatImageView7 = onView(
                allOf(withIndex(withId(R.id.imageView), 9), withContentDescription("Categoria"),
                        withParent(allOf(withId(R.id.cardview1),
                                withParent(withId(R.id.recycler_view1)))),
                        isDisplayed()));
        appCompatImageView7.perform(click());

        ViewInteraction relativeLayout7 = onView(
                allOf(childAtPosition(
                        withId(R.id.listViewResult),
                        0),
                        isDisplayed()));
        relativeLayout7.perform(click());

        ViewInteraction floatingActionButton19 = onView(
                allOf(withId(R.id.fabClose), isDisplayed()));
        floatingActionButton19.perform(click());

        ViewInteraction relativeLayout8 = onView(
                allOf(childAtPosition(
                        withId(R.id.listViewResult),
                        2),
                        isDisplayed()));
        relativeLayout8.perform(click());

        ViewInteraction appCompatButton12 = onView(
                allOf(withId(R.id.leftButton), isDisplayed()));
        appCompatButton12.perform(click());

        ViewInteraction appCompatButton13 = onView(
                allOf(withId(R.id.leftButton), isDisplayed()));
        appCompatButton13.perform(click());

        ViewInteraction floatingActionButton20 = onView(
                allOf(withId(R.id.fabClose), isDisplayed()));
        floatingActionButton20.perform(click());

        ViewInteraction floatingActionButton21 = onView(
                allOf(withClassName(is("com.github.clans.fab.FloatingActionButton")),
                        withParent(withId(R.id.menu_yellow)),
                        isDisplayed()));
        floatingActionButton21.perform(click());

        ViewInteraction floatingActionButton22 = onView(
                allOf(withId(R.id.fabNew),
                        withParent(withId(R.id.menu_yellow)),
                        isDisplayed()));
        floatingActionButton22.perform(click());

        ViewInteraction appCompatImageView8 = onView(
                allOf(withIndex(withId(R.id.imageView), 5), withContentDescription("Categoria"),
                        withParent(allOf(withId(R.id.cardview1),
                                withParent(withId(R.id.recycler_view1)))),
                        isDisplayed()));
        appCompatImageView8.perform(click());

        ViewInteraction relativeLayout9 = onView(
                allOf(childAtPosition(
                        withId(R.id.listViewResult),
                        3),
                        isDisplayed()));
        relativeLayout9.perform(click());

        ViewInteraction floatingActionButton23 = onView(
                allOf(withId(R.id.fabClose), isDisplayed()));
        floatingActionButton23.perform(click());

        ViewInteraction floatingActionButton24 = onView(
                allOf(withClassName(is("com.github.clans.fab.FloatingActionButton")),
                        withParent(withId(R.id.menu_yellow)),
                        isDisplayed()));
        floatingActionButton24.perform(click());

        ViewInteraction floatingActionButton25 = onView(
                allOf(withId(R.id.fabNew),
                        withParent(withId(R.id.menu_yellow)),
                        isDisplayed()));
        floatingActionButton25.perform(click());

        ViewInteraction appCompatImageView9 = onView(
                allOf(withIndex(withId(R.id.imageView), 1), withContentDescription("Categoria"),
                        withParent(allOf(withId(R.id.cardview1),
                                withParent(withId(R.id.recycler_view1)))),
                        isDisplayed()));
        appCompatImageView9.perform(click());
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }

    private static Matcher<View> withIndex(final Matcher<View> matcher, final int index) {
        return new TypeSafeMatcher<View>() {
            int currentIndex = 0;

            @Override
            public void describeTo(Description description) {
                description.appendText("with index: ");
                description.appendValue(index);
                matcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                return matcher.matches(view) && currentIndex++ == index;
            }
        };
    }
}
