package upone.com.br.cluba.user;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import upone.com.br.cluba.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.pressImeActionButton;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@SuppressWarnings("deprecation")
@LargeTest
@RunWith(AndroidJUnit4.class)
public class UserActivityTest {

    private static final int COUNT_TO_TEST = 10;

    @Rule
    public ActivityTestRule<UserActivity> mActivityTestRule = new ActivityTestRule<>(UserActivity.class);

    @Test
    public void userActivityTest() {
        for (int i = 0; i < COUNT_TO_TEST; i++) {

            ViewInteraction appCompatTextView = onView(
                    allOf(withId(R.id.textView), withText("Edvania"), withContentDescription("Garçom"),
                            withParent(allOf(withId(R.id.cardview1),
                                    withParent(withId(R.id.recycler_view1)))),
                            isDisplayed()));
            appCompatTextView.perform(click());

            ViewInteraction appCompatEditText = onView(
                    allOf(withId(R.id.num_bar_tab_et), isDisplayed()));
            appCompatEditText.perform(click());

            ViewInteraction appCompatEditText2 = onView(
                    allOf(withId(R.id.num_bar_tab_et), isDisplayed()));
            appCompatEditText2.perform(replaceText("104"), closeSoftKeyboard());

            ViewInteraction appCompatEditText3 = onView(
                    allOf(withId(R.id.num_bar_tab_et), withText("104"), isDisplayed()));
            appCompatEditText3.perform(pressImeActionButton());

            ViewInteraction appCompatImageView = onView(
                    allOf(withIndex(withId(R.id.imageView), 0), withContentDescription("Categoria"),
                            withParent(allOf(withId(R.id.cardview1),
                                    withParent(withId(R.id.recycler_view1)))),
                            isDisplayed()));
            appCompatImageView.perform(click());

            ViewInteraction relativeLayout = onView(
                    allOf(childAtPosition(
                            withId(R.id.listViewResult),
                            1),
                            isDisplayed()));
            relativeLayout.perform(click());

            ViewInteraction floatingActionButton = onView(
                    allOf(withId(R.id.fabClose), isDisplayed()));
            floatingActionButton.perform(click());

            ViewInteraction floatingActionButton2 = onView(
                    allOf(withClassName(is("com.github.clans.fab.FloatingActionButton")),
                            withParent(withId(R.id.menu_yellow)),
                            isDisplayed()));
            floatingActionButton2.perform(click());

            ViewInteraction floatingActionButton3 = onView(
                    allOf(withId(R.id.fabClose),
                            withParent(withId(R.id.menu_yellow)),
                            isDisplayed()));
            floatingActionButton3.perform(click());

            ViewInteraction appCompatButton = onView(
                    allOf(withId(android.R.id.button1), withText("OK"),
                            withParent(allOf(withClassName(is("android.widget.LinearLayout")),
                                    withParent(withClassName(is("android.widget.LinearLayout"))))),
                            isDisplayed()));
            appCompatButton.perform(click());
        }

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }

    private static Matcher<View> withIndex(final Matcher<View> matcher, final int index) {
        return new TypeSafeMatcher<View>() {
            int currentIndex = 0;

            @Override
            public void describeTo(Description description) {
                description.appendText("with index: ");
                description.appendValue(index);
                matcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                return matcher.matches(view) && currentIndex++ == index;
            }
        };
    }
}
